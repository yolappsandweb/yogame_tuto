var game = new Phaser.Game(800, 405, Phaser.AUTO, '', { preload: preload, create: create, update: update });
var platforms;
var pv1Text;
var pv2Text;
var user1;
var user2;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function lifeUpdate(user, nbPv) {
    //  Add and update the score
    user.pv -= nbPv;
    if (user.pv < 0) { user.pv = 0; };
    if (user.id == 1) { pv1Text.text = 'Life Player 1 : ' + user.pv + '%'; }
    else { pv2Text.text = user.pv + '% : Life Player 2'; };
}

function collidePlayers (user1, user2) {
    lifeUpdate(user1, 10);    
}

function cursorGesture(user) {
    user.modelPlayer.body.velocity.x = 0;

	if (user.leftButton.isDown)
    {
        //  Move to the left
        user.modelPlayer.body.velocity.x = -150;
        user.modelPlayer.animations.play('left');
    }
    else if (user.rightButton.isDown)
    {
        //  Move to the right
        user.modelPlayer.body.velocity.x = 150;
        user.modelPlayer.animations.play('right');
    }
    else if (user.attackButton.isDown)
    {
		lifeUpdate(user, 10);
	}
	else if (user.magicButton.isDown)
    {
	    lifeUpdate(user, 10);
	}
    else
    {
        user.modelPlayer.animations.stop();
        user.modelPlayer.frame = 4;
    }

    //  Allow the player to jump if they are touching the ground.
    if (user.upButton.isDown && user.modelPlayer.body.touching.down)
    {
        user.modelPlayer.body.velocity.y = -400;
    }

}

function preload() {
    game.load.image('background', 'textures/background.png');
    game.load.image('ground', 'textures/ground.png');
    game.load.spritesheet('dude', 'textures/dude.png', 28.5, 48);
}

function create() {
	//  We're going to be using physics, so enable the Arcade Physics system
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.add.sprite(0, 0, 'background');

    //  The platforms group contains the ground and the 2 ledges we can jump on
    platforms = game.add.group();
    //  We will enable physics for any object that is created in this group
    platforms.enableBody = true;
    // Here we create the ground.
    var ground = platforms.create(0, game.world.height - 30, 'ground');
    //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
    ground.scale.setTo(2, 2);
    //  This stops it from falling away when you jump on it
    ground.body.immovable = true;

	user1 = new Player(1, 175); 
    user2 = new Player(2, 625); 
    
    user1.addControls(
		Phaser.Keyboard.Z, 
		Phaser.Keyboard.S, 
		Phaser.Keyboard.Q, 
		Phaser.Keyboard.D,
    	Phaser.Keyboard.SPACEBAR,
    	Phaser.Keyboard.ALT);
    
    user2.addControls(
        Phaser.Keyboard.UP, 
        Phaser.Keyboard.DOWN, 
        Phaser.Keyboard.LEFT, 
        Phaser.Keyboard.RIGHT,
        Phaser.Keyboard.CONTROL,
        Phaser.Keyboard.NUMPAD_0);

    pv1Text = game.add.text(16, 16, 'Life Player 1 : '+user1.pv+'%', { fontSize: '24px', fill: '#000' });
    pv2Text = game.add.text(550, 16, user2.pv + '% : Life Player 2', { fontSize: '24px', fill: '#000' });
}

function update() {
	//Gestion des collisions entre les joueurs et le terrain
	user1.collideGesture(platforms);
	user2.collideGesture(platforms);
	user1.collideGesture(user2.modelPlayer);

	//Gestion des évènements d'entrée
	cursorGesture(user1);
    cursorGesture(user2);
}