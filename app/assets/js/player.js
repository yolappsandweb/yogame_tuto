class Player {
 constructor(id, marginLeft) {
    this.id = id;
    this.pv = 100;
    this.modelPlayer = game.add.sprite(marginLeft, game.world.height - 150, 'dude');
    this.cursor = game.input.keyboard.createCursorKeys();
    this.initPlayer();
  }

  initPlayer() {
	//  We need to enable physics on the player
	game.physics.arcade.enable(this.modelPlayer);

	//  Player physics properties. Give the little guy a slight bounce.
	this.modelPlayer.body.bounce.y = 0.1;
	this.modelPlayer.body.gravity.y = 800;
	this.modelPlayer.body.collideWorldBounds = true;

	//  Our two animations, walking left and right.
	this.modelPlayer.animations.add('left', [0, 1, 2, 3], 10, true);
	this.modelPlayer.animations.add('right', [5, 6, 7, 8], 10, true);
  }

  collideGesture(modelObject) { 
 	this.hitPlatform = game.physics.arcade.collide(this.modelPlayer, modelObject); 
  }
 
  addControls(upButton, downButton, rightButton, leftButton, attackButton, magicButton) {
	this.upButton = game.input.keyboard.addKey(upButton);
	this.downButton = game.input.keyboard.addKey(downButton);
	this.leftButton = game.input.keyboard.addKey(rightButton);
	this.rightButton = game.input.keyboard.addKey(leftButton);
	this.attackButton = game.input.keyboard.addKey(attackButton);
	this.magicButton = game.input.keyboard.addKey(magicButton);
  }
}