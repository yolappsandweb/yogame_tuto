module.exports = config:
  paths:
    "watched": ["app", "vendor"]
    "public": "public"
  files:
    javascripts:
      joinTo:
        'js/app.js': /^app/
        'js/vendor.js': /^vendor/
 
      order:
        before: []
 
    stylesheets: joinTo: 'css/style.css'
 

 
  plugins:
    autoReload:
      port: 9193
    uglify:
      mangle: false